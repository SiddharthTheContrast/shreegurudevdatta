import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class AudioProvider extends InheritedWidget {
  final AudioPlayer player;

  List allTracks = [
    {
      'id': 0,
      'name': 'shree_gurudev_datt_naam_jaap',
      'sname': '॥ श्री गुरूदेव दत्त ॥',
      'ext': '.mp3',
      'isPlaying': false,
      'type': 'naam'
    },
    {
      'id': 1,
      'name': 'digambara_digambara_dhoon',
      'sname': 'दिगंबरा दिगंबरा',
      'ext': '.mp3',
      'isPlaying': false,
      'type': 'naam'
    },
    {
      'id': 2,
      'name': 'guru_parampara_stotra',
      'sname': 'गुरूपरंपरास्तोत्र',
      'ext': '.mp3',
      'isPlaying': false,
      'type': 'stotra'
    },
    {
      'id': 3,
      'name': 'dakshina_murti_stotra',
      'sname': 'दक्षिणामूर्तीस्तोत्र',
      'ext': '.mp3',
      'isPlaying': false,
      'type': 'stotra'
    },
    {
      'id': 4,
      'name': 'Shyam_Varna',
      'sname': 'परमपूज्य श्री रामकृष्ण स्तोत्र',
      'ext': '.mp3',
      'isPlaying': false,
      'type': 'stotra'
    },
    {
      'id': 5,
      'name': 'gurudevanchi_aarti_1_final',
      'sname': 'श्री गुरुदेवांची आरती: १',
      'ext': '.mp3',
      'isPlaying': false,
      'type': 'stotra'
    },
    {
      'id': 6,
      'name': 'gurudevanchi_aarati 2_final',
      'sname': 'श्री गुरुदेवांची आरती: २',
      'ext': '.mp3',
      'isPlaying': false,
      'type': 'stotra'
    }
  ];

  AudioProvider({Key key, Widget child, this.player})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {

    return true;
  }

  static AudioProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(AudioProvider)
        as AudioProvider);
  }
}
