import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:localstorage/localstorage.dart';

String path = "http://www.sadguruwani.in/admin/";

class ServicesProvider extends InheritedWidget {
  dynamic todaysSadGuruvani;

  DateTime today = DateTime.now();

  final LocalStorage storage = new LocalStorage('sgd');

  List<dynamic> savs = [];

  var connectionStatus = 'Unknown';
  Connectivity connectivity;
  StreamSubscription<ConnectivityResult> subscription;

  ServicesProvider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    // TODO: implement updateShouldNotify
    return true;
  }

  static ServicesProvider of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(ServicesProvider)
        as ServicesProvider);
  }
  
  Future<List<dynamic>> getSadguruVanis() async {
    print("Getting Sadguruvanis");
    try {
      await storage.ready;
      var savs = await storage.getItem("sav");
      if (savs != null && savs != []) {
        // print(savs);
        this.savs = savs;
        return Future.value(savs);
      }

      var url = path + "rest/sav";
      var httpClient = http.Client();
      var response = await httpClient.read(url);
      List<dynamic> sadguruVanis = json.decode(response);
      this.savs = sadguruVanis;
      storage.setItem("sav", sadguruVanis);
      return Future.value(sadguruVanis);
    } catch (e) {
      print("$e");
      return Future.error(e);
    }
  }

  Future<List<dynamic>> getUtsavs() async {
    print("Getting Utsavs");
    try {
      var url = path + "rest/utsav";
      var httpClient = http.Client();
      var response = await httpClient.read(url);
      List<dynamic> utsavs = json.decode(response);
      storage.ready.then((value) {
        print("storage is ready $value");
        storage.setItem("utsavs", utsavs);
      });
      return Future.value(utsavs);
    } catch (e) {
      return Future.error(e);
    }
  }

  Future<List<dynamic>> postQuery(form) async {
    print("Posting Query");
    try {
      var url = path + "rest/node";
      var httpClient = http.Client();
      final response = await httpClient
          .post(url, body: form, headers: {'Content-Type': 'application/json'});
      List<dynamic> queryResponse = json.decode(response.body);
      print("$queryResponse");
      return Future.value(queryResponse);
    } catch (e) {
      return Future.error(e);
    }
  }

  retrieveSadguru() {
    return todaysSadGuruvani;
  }

  // Future<dynamic> retrieveUtsavs() async {
  //   //return JSON.parse(localStorage.getItem('utsavs'));
  //   try {
  //     return Future.value(storage.getItem("utsavs"));
  //   } catch (e) {
  //     return Future.error(e);
  //   }
  // }

  setTodaysSad() async {
    List allsavs;
    await storage.ready;
    // storage.getItem('sav').then((value) {
    allsavs = storage.getItem('sav');
    var date2 = DateTime.now();

    for (var i in allsavs) {
      date2 = DateTime.parse(i["date"]);
      if (date2.day == today.day &&
          (date2.month + 1) == (today.month + 1) &&
          date2.year == today.year) {
        print('match found');
        todaysSadGuruvani = i;
        break;
      }
    }
  }

  // sortDataByDate(data, customMonth, customYear) {
  //   if (data.length) {
  //     List sortedData = [];

  //     for (var i = 0; i <= data.length; i++) {
  //       var format = new DateFormat("M");
  //       var formatYear = new DateFormat("y");
  //       var month = format.format(DateTime.parse(data[i]['date']));
  //       var year = formatYear.format(DateTime.parse(data[i]['date']));
  //       if (month == customMonth && year == customYear) {
  //         sortedData.add(data[i]);
  //       }
  //     }
  //     return data;
  //   }
  // }

  Future<dynamic> getSavByDate(DateTime recvdDate) async {
    List allsavs;
    await storage.ready;
    // return storage.getItem('sav').then((value) {
    allsavs = storage.getItem('sav');

    var date2 = DateTime.now();
    var found = false;
    for (var i in allsavs) {
      date2 = DateTime.parse(i["date"]);
      if (date2.day == recvdDate.day &&
          (date2.month + 1) == (recvdDate.month + 1) &&
          date2.year == recvdDate.year) {
        print('match found');
        found = true;
        return Future.value(i);
      }
    }
    if (found == false) {
      return Future.value(null);
    }
    // });
  }
}
