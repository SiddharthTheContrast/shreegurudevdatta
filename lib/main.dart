import 'package:audioplayers/audioplayers.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'dart:collection';

import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shreegurudevdatta/Model/audio_provider.dart';
import 'package:shreegurudevdatta/View/feedback.dart';
import 'package:shreegurudevdatta/View/home.dart';
import 'package:shreegurudevdatta/Presenter/namjap.dart';
import 'package:shreegurudevdatta/View/sadguruvani.dart';
import 'package:shreegurudevdatta/Model/services.dart';
import 'package:shreegurudevdatta/Presenter/utils.dart';
import 'package:shreegurudevdatta/View/anukramanika.dart';
import 'package:shreegurudevdatta/View/popup.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // final LocalStorage storage = new LocalStorage('sgd');

  _exitApp(BuildContext context) {
    // AudioProvider.of(context).player.stop();
    print(AudioProvider.of(context));
    if (AudioProvider.of(context) != null) {
      print("disposing");
      print(AudioProvider.of(context).player);
      AudioProvider.of(context).player.stop();
      AudioProvider.of(context).player.release();
    }
    return;
  }

  @override
  Widget build(BuildContext context) {
    return ServicesProvider(
      child: AudioProvider(
        player: AudioPlayer(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Shree Gurudev Datta',
          theme: ThemeData(
              primarySwatch:
                  MaterialColor(Color.fromRGBO(140, 170, 216, 1).value, {
                50: Color.fromRGBO(140, 170, 216, 1),
                100: Color.fromRGBO(140, 170, 216, 1),
                200: Color.fromRGBO(140, 170, 216, 1),
                300: Color.fromRGBO(140, 170, 216, 1),
                400: Color.fromRGBO(140, 170, 216, 1),
                500: Color.fromRGBO(140, 170, 216, 1),
                600: Color.fromRGBO(140, 170, 216, 1),
                700: Color.fromRGBO(140, 170, 216, 1),
                800: Color.fromRGBO(140, 170, 216, 1),
                900: Color.fromRGBO(140, 170, 216, 1),
              }),
              primaryTextTheme:
                  Typography(platform: TargetPlatform.android).white),
          home: WillPopScope(onWillPop: _exitApp(context), child: MyHomePage()),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String localFilePath;
  DateTime mainDateTime = DateTime.now();
  int _currentIndex = 0;

  void onTabTapped(int index, [isAction = false, DateTime dateTime]) async {
    if (index != _currentIndex || isAction) {
      _history.add(index);
      setState(() => _currentIndex = index);
      Navigator.push(context, BottomNavigationRoute()).then((x) {
        _history.removeLast();
        setState(() => _currentIndex = _history.last);
      });
    } else if (dateTime != null && index == 3) {
      mainDateTime = dateTime;
      Navigator.pop(context, false);
      await Future.delayed(const Duration(milliseconds: 30));
      _history.add(index);
      setState(() => _currentIndex = index);
      Navigator.push(context, BottomNavigationRoute()).then((x) {
        _history.removeLast();
        setState(() => _currentIndex = _history.last);
      });
    }
  }

  @override
  void dispose() {
    ServicesProvider.of(context).subscription.cancel();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    ServicesProvider.of(context).connectivity = new Connectivity();
    ServicesProvider.of(context).subscription = ServicesProvider.of(context)
        .connectivity
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        ServicesProvider.of(context).connectionStatus = result.toString();
      });

      if (result == ConnectivityResult.none) {
        Fluttertoast.showToast(
            msg: "No network connection!",
            backgroundColor: Colors.black87,
            textColor: Colors.white);
      } else {
        Fluttertoast.showToast(
            msg: "Connected :)",
            backgroundColor: Colors.black87,
            textColor: Colors.white);
      }
    });
  }

  List<int> _history = [0];

  @override
  Widget build(BuildContext context) {
    // return FutureBuilder(
    //   future: ServicesProvider.of(context).storage.ready,
    //   builder: (BuildContext context, snapshot) {
    return FutureBuilder(
      future: ServicesProvider.of(context).getSadguruVanis(),
      builder: (BuildContext context, snapshot) {
        print(snapshot.connectionState);
        return ModalProgressHUD(
          opacity: .8,
          color: Colors.black,
          progressIndicator: Container(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Material(
                      type: MaterialType.transparency,
                      child: Text(
                        "Loading...",
                        style: TextStyle(color: Colors.grey[200], fontSize: 18),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          child: appScaffold(),
          inAsyncCall: snapshot.connectionState != ConnectionState.done &&
              (ServicesProvider.of(context).savs == null ||
                  ServicesProvider.of(context).savs.isEmpty),
        );
      },
    );
    //   },
    // );
  }

  Widget appScaffold() {
    getPopupData();

    final List<Widget> _children = [
      new HomePageState(onTabSelect: onTabTapped),
      Icon(Icons.directions_transit),
      new Namjap(),
      new SadguruvaniState(dateTime: mainDateTime),
      new FeedbackPage(),
      new Anukramanika(onTabSelect: onTabTapped)
    ];

    var tabButtons = [
      {
        "icon": Icons.home,
        "name": 'मुख्यपृष्ठ',
        "title": '॥ श्री गुरूदेव दत्त ॥',
        "actions": [
          IconButton(
            icon: Icon(Icons.live_help),
            color: Colors.white,
            onPressed: () {
              onTabTapped(4);
            },
          )
        ]
      },
      {
        "icon": Icons.surround_sound,
        "name": 'नामजप',
        "title": 'नामजप',
        "actions": null
      },
      {
        "icon": Icons.music_note,
        "name": 'स्तोत्रे / आरती',
        "title": 'स्तोत्रे / आरती',
        "actions": null
      },
      {
        "icon": Icons.calendar_today,
        "name": 'सद्गुरूवाणी',
        "title": 'सद्गुरूवाणी',
        "actions": [iconButton(), containerPopupMenuButton()]
      },
      {
        "icon": Icons.help_outline,
        "name": 'अभिप्राय',
        "title": 'अभिप्राय',
        "actions": null
      },
      {
        "icon": Icons.subject,
        "name": 'अनुक्रमणिका',
        "title": 'अनुक्रमणिका',
        "actions": null
      },
    ];
    var children = _children;
    return Scaffold(
        key: _scaffoldKey,
        bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            onTap: onTabTapped,
            currentIndex: _currentIndex < 4 ? _currentIndex : 0,
            items: tabButtons
                .take(4)
                .map(
                  (item) => BottomNavigationBarItem(
                      icon: Icon(item['icon']),
                      title: Text(item['name'], textAlign: TextAlign.center)),
                )
                .toList()),
        appBar: AppBar(
            centerTitle: true,
            title: Text(tabButtons[_currentIndex]['title'],
                textScaleFactor: 1.3, textAlign: TextAlign.center),
            actions: tabButtons[_currentIndex]['actions'],
            leading: _currentIndex != 0
                ? IconButton(
                    icon: Icon(Icons.arrow_back),
                    color: Colors.white,
                    // iconSize: 35,
                    onPressed: () => Navigator.pop(context, false),
                  )
                : null),
        body: ServicesProvider.of(context).connectionStatus ==
                ConnectivityResult.none.toString()
            ? children[_currentIndex]
            : children[_currentIndex]);
  }

  List data;
  LinkedHashMap<String, String> mapData = new LinkedHashMap();
  Future<void> getPopupData() async {
    await ServicesProvider.of(context).getUtsavs().then((list) {
      data = list;
      for (int i = 0; i < data.length; i++) {
        mapData[data[i]['date']] = data[i]['utsav'];
      }
    });
  }

  IconButton containerPopupMenuButton() {
    return IconButton(
      icon: Icon(Icons.arrow_drop_down),
      color: Colors.white,
      onPressed: () {
        _showPopupDialog();
      },
    );
  }

  void _showPopupDialog() {
    List<String> titleList = mapData.values.toList();
    String _checkedTitle;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
                contentPadding: EdgeInsets.all(0.0),
                title: Text('उत्सव निवडा',
                    style: TextStyle(fontSize: 16.0, color: Colors.grey)),
                children: <Widget>[
                  RadioButtonGroup(
                    orientation: GroupedButtonsOrientation.VERTICAL,
                    margin: const EdgeInsets.all(0.0),
                    onSelected: (String selected) => setState(() {
                          _checkedTitle = selected;
                          String dateKey = mapData.keys.firstWhere(
                              (k) => mapData[k] == _checkedTitle,
                              orElse: () => null);
                          if (dateKey != null) {
                            DateTime datetime =
                                DateTime.fromMillisecondsSinceEpoch(
                                    DateTime.parse(dateKey)
                                        .millisecondsSinceEpoch);
                            print('tap: datetime: $datetime');
                            Navigator.of(context, rootNavigator: true).pop();
                            onTabTapped(3, false, datetime);
                          }
                        }),
                    labels: titleList,
                    picked: _checkedTitle,
                    itemBuilder: (Radio rb, Text txt, int i) {
                      return ListView(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                rb,
                                txt,
                              ],
                            ),
                            Divider()
                          ]);
                    },
                  )
                ]);
      },
    );
  }

  IconButton iconButton() {
    return IconButton(
      icon: Icon(Icons.subject),
      color: Colors.white,
      onPressed: () {
        onTabTapped(5);
      },
    );
  }
  // void choiceAction(String choice){
  //   print('You click on app');
  // }  on Select function
}
