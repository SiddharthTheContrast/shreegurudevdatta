import 'package:flutter/material.dart';
import 'package:shreegurudevdatta/Presenter/utils.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePageState extends StatefulWidget {
  HomePageState({this.onTabSelect});

  final TabChangeCallback onTabSelect;

  @override
  _HomePageStateState createState() => _HomePageStateState();
}

class _HomePageStateState extends State<HomePageState> {
  List<String> homeButtons = [
    'सद्गुरूंचे जीवन आणि कार्य',
    'नामजप',
    'स्तोत्रे / आरती',
    'सद्गुरूवाणी',
    'अधिक माहिती'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(margin: EdgeInsets.symmetric(vertical: 10)),
            Image.asset('assets/img/maharaj1.png'),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
              child: Column(
                  children: homeButtons
                      .map((item) => Row(
                            children: [
                              Expanded(
                                child: Container(
                                  height: 55,
                                  margin: EdgeInsets.symmetric(vertical: 8),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  child: RaisedButton(
                                    onPressed: () async {
                                      bool isLast = homeButtons.indexOf(item) ==
                                          (homeButtons.length - 1);

                                      if (isLast) {
                                        const url =
                                            'http://shriramkrishnakshirsagarmaharaj.com/';
                                        if (await canLaunch(url)) {
                                          await launch(url);
                                        } else {
                                          throw 'Could not launch $url';
                                        }
                                      } else {
                                        widget.onTabSelect(
                                            homeButtons.indexOf(item));
                                      }
                                    },
                                    textColor: Colors.white,
                                    color: Color.fromRGBO(140, 170, 216, 1),
                                    child:
                                        Text(item, textAlign: TextAlign.center),
                                  ),
                                ),
                              )
                            ],
                          ))
                      .toList()),
            )
          ],
        ),
      ),
    );
  }
}
