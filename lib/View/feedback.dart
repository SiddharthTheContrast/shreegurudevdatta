import 'package:flutter/material.dart';

class FeedbackPage extends StatefulWidget {
  @override
  _FeedbackPageState createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  @override
  Widget build(BuildContext context) {
    return FeedbackForm();
  }
}

class FeedbackForm extends StatefulWidget {
  @override
  _FeedbackFormState createState() => _FeedbackFormState();
}

class _FeedbackFormState extends State<FeedbackForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey we created above

    onFormSubmit(FormState currentState) {
      // Validate will return true if the form is valid, or false if
      // the form is invalid.
      if (currentState != null && currentState.validate()) {
        // If the form is valid, we want to show a Snackbar
        
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text('Processing Data')));
      }
    }

    InputDecoration fieldDecoration(label, hint) {
      return InputDecoration(
        // hintText: hint,
        labelText: label,
      );
    }

    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                decoration: fieldDecoration("पूर्ण नाव", "पूर्ण नाव"),
                onFieldSubmitted: (value) {
                  onFormSubmit(_formKey.currentState);
                },    
                keyboardType: TextInputType.text,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'नाव आवश्यक आहे !';
                  }
                },
              ),
              TextFormField(
                decoration: fieldDecoration("ई-मेल", "ई-मेल"),
                onFieldSubmitted: (value) {
                  onFormSubmit(_formKey.currentState);
                },
                keyboardType: TextInputType.emailAddress,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'किमान ई-मेल किंवा नाव आवश्यक आहे ! ई-मेल वैध नाही !';
                  }
                },
              ),
              TextFormField(
                decoration: fieldDecoration("दूरध्वनी क्र.", "दूरध्वनी क्र."),
                onFieldSubmitted: (value) {
                  onFormSubmit(_formKey.currentState);
                },
                keyboardType: TextInputType.phone,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'कृपया १० अंकी क्र. टाका !';
                  }
                },
              ),
              TextFormField(
                decoration: fieldDecoration("अभिप्राय", "अभिप्राय"),
                onFieldSubmitted: (value) {
                  onFormSubmit(_formKey.currentState);
                },
                keyboardType: TextInputType.multiline,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'अभिप्राय आवश्यक आहे !';
                  }
                },
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      height: 55,
                      margin: EdgeInsets.symmetric(vertical: 30),
                      child: RaisedButton(
                        onPressed: () async {
                          onFormSubmit(_formKey.currentState);
                        },
                        textColor: Colors.white,
                        color: Color.fromRGBO(140, 170, 216, 1),
                        child: Text("सादर", textAlign: TextAlign.center),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
