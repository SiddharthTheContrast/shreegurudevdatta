import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:localstorage/localstorage.dart';
import 'package:shreegurudevdatta/Model/services.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shreegurudevdatta/Presenter/utils.dart';

class Anukramanika extends StatefulWidget {
  Anukramanika({this.onTabSelect});
  final TabChangeCallback onTabSelect;
  @override
  _AnukramanikaState createState() {
    return _AnukramanikaState();
  }
}

class _AnukramanikaState extends State<Anukramanika> {
  final String url = "http://www.sadguruwani.in/admin/";
  List data, tempData, storedData;
  List<String> monthNames = [
    'जानेवारी',
    'फेब्रुवारी',
    'मार्च',
    'एप्रिल',
    'मे',
    'जून',
    'जुलै',
    'ऑगस्ट',
    'सप्टेंबर',
    'ऑक्टोबर',
    'नोव्हेंबर',
    'डिसेंबर'
  ];
  List<String> yearNumbers = [];
  var isLoading = true;
  int selectedMonth, selectedYear, lastYear;
  String selectedMonthDropdown = '',
      selectedYearDropdown = '',
      day,
      month,
      year;

  LocalStorage storage = new LocalStorage("sgd");
  @override
  void initState() {
    super.initState();
    tempData = null;
    storedData=null;
    _getYearNumbers();
    _getCurrentDateTime();
    if (storage.getItem("events") != null) {
      storedData = storage.getItem("events");
    }
    this.getjsondata();
  }

  void _getCurrentDateTime() {
    selectedMonth = DateTime.now().month;
    selectedYear = yearNumbers.indexOf(DateTime.now().year.toString());
    setState(() {
      selectedMonthDropdown = '';
      selectedYearDropdown = '';
    });
  }

  void _getYearNumbers() {
    lastYear = DateTime.now().year;
    for (int i = 2016; i <= lastYear; i++) {
      yearNumbers.add(i.toString());
    }
  }

  Future<String> getjsondata() async {
    var url = path + 'rest/sav';
    var response = await http.get(
      Uri.encodeFull(url),
    );

    setState(() {
      isLoading = false;
      var converts = json.decode(response.body);
      data = converts;
      tempData = data;
      storage.setItem("events",tempData);
    });
  }

  void fetchData(int selectedMonth1, int selectedYear1) {
    selectedMonth = selectedMonth1;
    selectedYear = selectedYear1;
    var sortedArr = [];
    var count = 0;
    for (var item in data) {
      var format = new DateFormat("M");
      var formatYear = new DateFormat("y");
      var month = format.format(DateTime.parse(item['date']));
      var year = formatYear.format(DateTime.parse(item['date']));
      if (month == selectedMonth.toString() &&
          year == yearNumbers[selectedYear]) {
        // print(
        //     'month: $month selectedMonth: ${selectedMonth.toString()} year: $year selectedYear: ${selectedYear.toString()}');
        sortedArr.insert(count, item);
        count = count + 1;
      }
    }
    setState(() {
      selectedMonthDropdown = monthNames[selectedMonth - 1];
      selectedYearDropdown = yearNumbers[selectedYear];
      tempData = sortedArr;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              margin: EdgeInsets.only(
                left: 30.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    child: Row(
                      children: <Widget>[
                        selectedMonthDropdown == ''
                            ? Text(
                                'महिना',
                                style: TextStyle(
                                    color: Colors.grey, fontSize: 16.0),
                              )
                            : Text(
                                selectedMonthDropdown,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16.0),
                              ),
                        Container(
                          width: 40.0,
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_drop_down),
                          onPressed: null,
                        ),
                      ],
                    ),
                    onTap: () {
                      _showMonthAlertDialog(context);
                    },
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    child: Row(
                      children: <Widget>[
                        selectedYearDropdown == ''
                            ? Text(
                                'वर्ष',
                                style: TextStyle(
                                    color: Colors.grey, fontSize: 16.0),
                              )
                            : Text(
                                selectedYearDropdown,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16.0),
                              ),
                        Container(
                          width: 40.0,
                        ),
                        IconButton(
                          icon: Icon(
                            Icons.arrow_drop_down,
                          ),
                          onPressed: null,
                        ),
                      ],
                    ),
                    onTap: () {
                      _showYearAlertDialog(context);
                    },
                  ),
                  Container(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            _getCurrentDateTime();
                            tempData = data;
                          });
                        },
                        icon: Icon(Icons.clear,
                            color: Color.fromRGBO(140, 170, 216, 1),
                            size: 15.0),
                      )),
                ],
              ),
            ),
          ),
          //add ListView Builder here
          Expanded(
            flex: 8,
            child: tempData == null
                ? storedData==null?
                Center(child: CircularProgressIndicator()):
                Card(
                    child: ListView.builder(
                      itemCount: storedData == null ? 0 : storedData.length,
                      itemBuilder: (context, index) {
                        DateTime datetime = DateTime.fromMillisecondsSinceEpoch(
                            DateTime.parse(storedData[index]['date'])
                                .millisecondsSinceEpoch);
                        day = datetime.day.toString();
                        day = day.length < 2 ? '0$day' : day;
                        month = datetime.month.toString();
                        month = month.length < 2 ? '0$month' : month;
                        year = datetime.year.toString();
                        return ListTile(
                            title: Column(children: <Widget>[
                          GestureDetector(
                              child: Row(
                                children: <Widget>[
                                  Text('$day/$month/$year',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 16.0)),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  Flexible(
                                      fit: FlexFit.loose,
                                      child: Text(
                                        storedData != null
                                            ? storedData[index]["Title"]
                                            : "",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                        ),
                                        textAlign: TextAlign.left,
                                      )),
                                ],
                              ),
                              onTap: () {
                                widget.onTabSelect(3, false, datetime);
                              }),
                          Divider(),
                        ]));
                      },
                    ),
                  )                
                : Card(
                    child: ListView.builder(
                      itemCount: tempData == null ? 0 : tempData.length,
                      itemBuilder: (context, index) {
                        DateTime datetime = DateTime.fromMillisecondsSinceEpoch(
                            DateTime.parse(tempData[index]['date'])
                                .millisecondsSinceEpoch);
                        day = datetime.day.toString();
                        day = day.length < 2 ? '0$day' : day;
                        month = datetime.month.toString();
                        month = month.length < 2 ? '0$month' : month;
                        year = datetime.year.toString();
                        return ListTile(
                            title: Column(children: <Widget>[
                          GestureDetector(
                              child: Row(
                                children: <Widget>[
                                  Text('$day/$month/$year',
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 16.0)),
                                  SizedBox(
                                    width: 5.0,
                                  ),
                                  Flexible(
                                      fit: FlexFit.loose,
                                      child: Text(
                                        tempData != null
                                            ? tempData[index]["Title"]
                                            : "",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                        ),
                                        textAlign: TextAlign.left,
                                      )),
                                ],
                              ),
                              onTap: () {
                                widget.onTabSelect(3, false, datetime);
                              }),
                          Divider(),
                        ]));
                      },
                    ),
                  ),
          )
        ],
      ),
    );
  }

  Future<void> _showYearAlertDialog(BuildContext context) {
    String checkedYear = yearNumbers[0];
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            content: YearDialogContent(
          yearNumbers: yearNumbers,
          selectedMonth: selectedMonth,
          selectedYear: selectedYear,
          day: day,
          month: month,
          year: year,
          checkedYear: checkedYear,
          fetchData: fetchData,
        ));
      },
    );
  }

  Future<void> _showMonthAlertDialog(BuildContext context) {
    String checkedMonth = monthNames[0];
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            content: MonthDialogContent(
                monthNames: monthNames,
                selectedMonth: selectedMonth,
                selectedYear: selectedYear,
                day: day,
                month: month,
                checkedMonth: checkedMonth,
                fetchData: fetchData));
      },
    );
  }
}

class YearDialogContent extends StatefulWidget {
  final Function fetchData;
  List<String> yearNumbers;
  int selectedMonth, selectedYear;
  String day, month, year, checkedYear;
  YearDialogContent(
      {Key key,
      this.yearNumbers,
      this.selectedMonth,
      this.selectedYear,
      this.day,
      this.month,
      this.year,
      this.checkedYear,
      this.fetchData})
      : super(key: key);

  @override
  _YearDialogContentState createState() => new _YearDialogContentState();
}

class _YearDialogContentState extends State<YearDialogContent> {
  List<String> yearNumbers;
  int selectedMonth, selectedYear;
  String day, month, year;
  String checkedYear;
  Function fetchData;
  @override
  void initState() {
    super.initState();
    yearNumbers = widget.yearNumbers;
    selectedMonth = widget.selectedMonth;
    selectedYear = widget.selectedYear;
    day = widget.day;
    month = widget.month;
    year = widget.year;
    checkedYear = yearNumbers[selectedYear];
    fetchData = widget.fetchData;
  }

  _getContent() {
    if (widget.yearNumbers.length == 0) {
      return new Container();
    }

    return Container(
      width: double.maxFinite,
      height: 300,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(top: 3.5, bottom: 3.5),
              child: Text(
                'वर्ष',
                style: TextStyle(fontSize: 23.0),
              )),
          Divider(
            indent: 0.0,
          ),
          Container(
              transform: Matrix4.translationValues(-15.0, 0.0, 0.0),
              alignment: Alignment.centerLeft,
              width: double.maxFinite,
              height: 200.0,
              child: ListView(children: <Widget>[
                RadioButtonGroup(
                  orientation: GroupedButtonsOrientation.VERTICAL,
                  margin: const EdgeInsets.only(left: 0.0),
                  onSelected: (String selected) => setState(() {
                        checkedYear = selected;
                      }),
                  labels: yearNumbers,
                  picked: checkedYear,
                  itemBuilder: (Radio rb, Text txt, int i) {
                    return Row(
                      children: <Widget>[
                        rb,
                        txt,
                      ],
                    );
                  },
                )
              ])),
          Divider(
            indent: 0.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                color: Colors.white,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                child: Text('रद्द',
                    style: TextStyle(color: Color.fromRGBO(140, 170, 216, 1))),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                color: Colors.white,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                child: Text('निवडा',
                    style: TextStyle(color: Color.fromRGBO(140, 170, 216, 1))),
                onPressed: () {
                  selectedYear = yearNumbers.indexOf(checkedYear);
                  setState(() {});
                  Navigator.of(context).pop();
                  fetchData(selectedMonth, selectedYear);
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _getContent();
  }
}

class MonthDialogContent extends StatefulWidget {
  final Function fetchData;
  List<String> monthNames;
  int selectedMonth, selectedYear;
  String day, month, year, checkedMonth;
  MonthDialogContent(
      {Key key,
      this.monthNames,
      this.selectedMonth,
      this.selectedYear,
      this.day,
      this.month,
      this.year,
      this.checkedMonth,
      this.fetchData})
      : super(key: key);

  @override
  _MonthDialogContentState createState() => new _MonthDialogContentState();
}

class _MonthDialogContentState extends State<MonthDialogContent> {
  List<String> monthNames;
  int selectedMonth, selectedYear;
  String day, month, year;
  String checkedMonth;
  Function fetchData;
  @override
  void initState() {
    super.initState();
    monthNames = widget.monthNames;
    selectedMonth = widget.selectedMonth;
    selectedYear = widget.selectedYear;
    day = widget.day;
    month = widget.month;
    year = widget.year;
    checkedMonth = monthNames[selectedMonth - 1];
    fetchData = widget.fetchData;
  }

  _getContent() {
    if (widget.monthNames.length == 0) {
      return new Container();
    }

    return Container(
       width: double.maxFinite,
        height: 400.0,
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 3.5, bottom: 3.5),
                child: Text(
                  'महिना',
                  style: TextStyle(fontSize: 23.0),
                )),
            Divider(
              indent: 0.0,
            ),
            Container(
                transform: Matrix4.translationValues(-15.0, 0.0, 0.0),
                alignment: Alignment.centerLeft,
                width: double.maxFinite,
                height: 300.0,
                child: ListView(children: <Widget>[
                  RadioButtonGroup(
                    orientation: GroupedButtonsOrientation.VERTICAL,
                    margin: const EdgeInsets.only(left: 0.0),
                    onSelected: (String selected) => setState(() {
                          checkedMonth = selected;
                        }),
                    labels: monthNames,
                    picked: checkedMonth,
                    itemBuilder: (Radio rb, Text txt, int i) {
                      return Row(
                        children: <Widget>[
                          rb,
                          txt,
                        ],
                      );
                    },
                  )
                ])
                // child: ListView.builder(
                //   shrinkWrap: true,
                //   itemCount: monthNames.length,
                //   itemBuilder: (context, position) {
                //     return RadioListTile(
                //       controlAffinity: ListTileControlAffinity.platform,
                //       value: monthNames[position],
                //       groupValue: checkedMonth,
                //       title: Text(
                //         monthNames[position],
                //         textAlign: TextAlign.left,
                //       ),
                //       onChanged: (value) {
                //         setState(() {
                //           checkedMonth = monthNames[monthNames.indexOf(value)];
                //         });
                //         print('val $value no: ${monthNames.indexOf(value)}');
                //       },
                //       selected: checkedMonth == monthNames[position],
                //     );
                //   },
                // )
                ),
            Divider(
              indent: 0.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  color: Colors.white,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  child: Text('रद्द',
                      style:
                          TextStyle(color: Color.fromRGBO(140, 170, 216, 1))),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  color: Colors.white,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  child: Text('निवडा',
                      style:
                          TextStyle(color: Color.fromRGBO(140, 170, 216, 1))),
                  onPressed: () {
                    selectedMonth = monthNames.indexOf(checkedMonth) + 1;
                    print('selectedMonth: $selectedMonth');
                    Navigator.of(context).pop();
                    fetchData(selectedMonth, selectedYear);
                  },
                ),
              ],
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return _getContent();
  }
}
