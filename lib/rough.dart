import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';

String path = "http://www.sadguruwani.in/admin/";

class Anukramanika extends StatefulWidget {
  @override
  _AnukramanikaState createState() => _AnukramanikaState();
}

class _AnukramanikaState extends State<Anukramanika> {
  @override
  Widget build(BuildContext context) {
    return Anu();
  }
}

class Anu extends StatefulWidget {
  @override
  _AnuState createState() => _AnuState();
}

class _AnuState extends State<Anu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: new Row(
          children: <Widget>[
            new OutlineButton(
              child: Text('Select Month & Year.',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: Color.fromRGBO(140, 170, 216, 1))),
              onPressed: () {
                showPickerNumber(context);
              },
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(20.0),
              ),
              color: Colors.white,
              highlightColor: Colors.white,
              highlightedBorderColor: Color.fromRGBO(140, 170, 216, 1),
              highlightElevation: 10.0,
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.cancel,
                color: Color.fromRGBO(140, 170, 216, 1),
              ),
              highlightColor: Colors.white,
            ),
          ],
        ),
      ),
    );
  }

  showPickerNumber(BuildContext context) {
    Picker(
        adapter: NumberPickerAdapter(data: [
          NumberPickerColumn(begin: 1, end: 12),
          NumberPickerColumn(begin: 2015, end: 2019),
        ]),
        delimiter: [
          PickerDelimiter(
              child: Container(
            width: 10.0,
            alignment: Alignment.center,
            child: Icon(Icons.more_vert),
          ))
        ],
        hideHeader: true,
        title: Text("Please Select"),
        onConfirm: (Picker picker, List value) {
          print(value.toString());
          print(picker.getSelectedValues());
        }).showDialog(context);
  }
}
