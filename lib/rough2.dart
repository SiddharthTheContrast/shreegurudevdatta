import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shreegurudevdatta/Model/services.dart';

class SadguruvaniState extends StatefulWidget {
  @override
  _SadguruvaniStateState createState() => _SadguruvaniStateState();
}

class _SadguruvaniStateState extends State<SadguruvaniState> {
  DateTime selectedDate = DateTime.now();
  var viewSav;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await ServicesProvider.of(context).setTodaysSad();
    setState(() {
      viewSav = ServicesProvider.of(context).todaysSadGuruvani;
    });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2016, 12),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate) {
      viewSav = await ServicesProvider.of(context).getSavByDate(picked);
      setState(() {
        selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Card(
              margin: EdgeInsets.all(0),
              shape: BeveledRectangleBorder(
                borderRadius: BorderRadius.zero,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Text(new DateFormat('EEEE, MMMM dd, yyyy')
                              .format(selectedDate)),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 1),
                        ),
                        Container(
                          child: Text(viewSav != null ? viewSav["tithi"] : "",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 1),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                      child: OutlineButton(
                        onPressed: () => _selectDate(context),
                        child: Text('तारीख निवडा'),
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                        borderSide: BorderSide(color: Colors.blue),
                        textColor: Color.fromRGBO(140, 170, 216, 1),
                        color: Color.fromRGBO(140, 170, 216, 1),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 9,
            child: SingleChildScrollView(
              child: GestureDetector(
                onPanEnd: (details) async {
                  if (details.velocity.pixelsPerSecond.dx > 0) {
                    print("previous");
                    var prevDate = selectedDate.subtract(Duration(days: 1));
                    viewSav = await ServicesProvider.of(context)
                        .getSavByDate(prevDate);
                    setState(() {
                      selectedDate = prevDate;
                    });
                  } else {
                    print("next");
                    var nextDate = selectedDate.add(Duration(days: 1));
                    if(!nextDate.isAfter(DateTime.now())){
                      viewSav = await ServicesProvider.of(context)
                          .getSavByDate(nextDate);
                      setState(() {
                        selectedDate = nextDate;
                      });
                    }
                  }
                },
                child: Container(
                  margin: EdgeInsets.all(18),
                  child: Column(
                    children: <Widget>[
                      Container(height: 10),
                      Text(
                        viewSav != null
                            ? (viewSav["Title"] != null ? viewSav["Title"] : "")
                            : "",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(140, 170, 216, 1)),
                        textScaleFactor: 1.7,
                        textAlign: TextAlign.center,
                      ),
                      Container(height: 30),
                      Text(
                        viewSav != null
                            ? (viewSav["sadguruvani"] != null
                                ? viewSav["sadguruvani"]
                                : "")
                            : "",
                        textScaleFactor: 1.2,
                        textAlign: TextAlign.justify,
                      ),
                      Container(height: 20),
                      Divider(indent: 0, height: 30, color: Colors.black54),
                      Container(height: 20),
                      Text(
                        viewSav != null
                            ? (viewSav["Vishesh"] != null
                                ? viewSav["Vishesh"]
                                : "")
                            : "",
                        textScaleFactor: 1.2,
                        style: TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.justify,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
