import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:shreegurudevdatta/Model/audio_provider.dart';

class Namjap extends StatefulWidget {
  @override
  _NamjapState createState() => _NamjapState();
}

class _NamjapState extends State<Namjap> {
  AudioPlayer currentPlayer;
  final AudioCache audioCache = new AudioCache(prefix: 'music/');

  List allTracks;
  bool onLoop = false;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    allTracks = AudioProvider.of(context).allTracks;
    currentPlayer = AudioProvider.of(context).player;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // if (currentPlayer != null) {
    //   currentPlayer.release();
    //   currentPlayer.stop();
    // }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomAppBar(   
          elevation: 50.0,       
          child: Row(children: <Widget>[
                  Checkbox(
                    value: onLoop,
                    onChanged: (bool value) {
                      setState(() {
                        onLoop = !onLoop;
                      });
                    },
                  ), // Padding(padding: EdgeInsets.only(left: 10.0), child:
                  Text('पुनरावृत्ती ठेवा',
                      style: TextStyle(color: Colors.black, fontSize: 16.0))
                ])
        ),
        body: Column(children: <Widget>[
          Expanded(
              flex: 7,
              child: ListView(
                children: List.generate(allTracks.length, (i) {
                  return Card(
                    margin:
                        const EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 8,
                              child: Text(
                                allTracks.elementAt(i)['sname'],
                                textScaleFactor: 1.3,
                              )),
                          Expanded(
                              child: InkWell(
                                  onTap: () async {
                                    // audioCache.clearCache();
                                    if (currentPlayer != null) {
                                      currentPlayer.release();
                                      currentPlayer.stop();
                                    }

                                    if (allTracks.elementAt(i)['isPlaying']) {
                                      setState(() {
                                        allTracks.elementAt(i)['isPlaying'] =
                                            false;
                                      });
                                      return;
                                    }

                                    currentPlayer = await audioCache.play(
                                        allTracks.elementAt(i)['name'] +
                                            allTracks.elementAt(i)['ext']);

                                    if (currentPlayer.state ==
                                        AudioPlayerState.PLAYING) {
                                      currentPlayer
                                          .setReleaseMode(ReleaseMode.STOP);
                                      setState(() {
                                        allTracks.forEach((f) {
                                          f['isPlaying'] = false;
                                        });
                                        allTracks.elementAt(i)['isPlaying'] =
                                            true;
                                      });
                                    }
                                  },
                                  child: allTracks.elementAt(i)['isPlaying']
                                      ? Icon(Icons.pause, size: 30)
                                      : Icon(Icons.play_arrow, size: 30)))
                        ],
                      ),
                    ),
                  );
                }),
              )),
          // Column(children: <Widget>[
          //           Divider(color: Color.fromRGBO(140, 170, 216, 1)),
          //           Container(
          //               transform: Matrix4.translationValues(0.0, -15.0, 0.0),
          //               child: Row(children: <Widget>[
          //                 Checkbox(
          //                   value: onLoop,
          //                   onChanged: (bool value) {
          //                     setState(() {
          //                       onLoop = !onLoop;
          //                     });
          //                   },
          //                 ), // Padding(padding: EdgeInsets.only(left: 10.0), child:
          //                 Text('पुनरावृत्ती ठेवा',
          //                     style: TextStyle(color: Colors.black, fontSize: 16.0))
          //                 // )
          //               ]))
          //         ]),
        ]));
  }
}