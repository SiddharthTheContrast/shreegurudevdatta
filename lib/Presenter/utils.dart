
import 'package:flutter/material.dart';

class BottomNavigationRoute extends Route with LocalHistoryRoute {}

typedef TabChangeCallback = void Function(int index, [bool isAction, DateTime dateTime]);