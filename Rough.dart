import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:localstorage/localstorage.dart';
import 'package:shreegurudevdatta/Model/services.dart';

class Anukramanika extends StatefulWidget {
  @override
  _AnukramanikaState createState() => _AnukramanikaState();
}

class _AnukramanikaState extends State<Anukramanika> {
  @override
  Widget build(BuildContext context) {
    return Anu();
  }
}

class Anu extends StatefulWidget {
  @override
  _AnuState createState() => _AnuState();
}

class _AnuState extends State<Anu> {
    DateTime selectedDate = DateTime.now();
  var viewSav;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await ServicesProvider.of(context).setTodaysSad();
    setState(() {
      viewSav = ServicesProvider.of(context).todaysSadGuruvani;
    });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2016, 12, 13),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate) {
      viewSav = await ServicesProvider.of(context).getSavByDate(picked);
      setState(() {
        selectedDate = picked;
      });
    }
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Card(
              margin: EdgeInsets.all(0),
              shape: BeveledRectangleBorder(
                borderRadius: BorderRadius.zero,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                      
                      child: OutlineButton(
                        onPressed: () => _selectDate(context),
                        child: Text('तारीख निवडा'),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(140, 170, 216, 1)),
                        textColor: Color.fromRGBO(140, 170, 216, 1),
                        color: Color.fromRGBO(140, 170, 216, 1),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 9,
            child: SingleChildScrollView(
              child: GestureDetector(
                child: Container(
                  margin: EdgeInsets.all(18),
                  child: Column(
                    children: <Widget>[
                      Container(height: 10),
                      Text(
                        viewSav != null
                            ? (viewSav["Title"] != null ? viewSav["Title"] : "")
                            : "",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color.fromRGBO(140, 170, 216, 1)),
                        textScaleFactor: 1.7,
                        textAlign: TextAlign.center,
                      ),
                      Container(height: 30),
                      Text(
                        viewSav != null
                            ? (viewSav["sadguruvani"] != null
                                ? viewSav["sadguruvani"]
                                : "")
                            : "",
                        textScaleFactor: 1.2,
                        textAlign: TextAlign.justify,
                      ),
                      Container(height: 20),
                      Divider(indent: 0, height: 30, color: Colors.black54),
                      Container(height: 20),
                      Text(
                        viewSav != null
                            ? (viewSav["Vishesh"] != null
                                ? viewSav["Vishesh"]
                                : "")
                            : "",
                        textScaleFactor: 1.2,
                        style: TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.justify,
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
  

  //Custom picker
  // showPickerNumber(BuildContext context) {
  //   Picker(
  //       adapter: NumberPickerAdapter(data: [
  //         NumberPickerColumn(begin: 1, end: 12),
  //         NumberPickerColumn(begin: 2015, end: 2019),
  //       ]),
  //       delimiter: [
  //         PickerDelimiter(
  //             child: Container(
  //           width: 10.0,
  //           alignment: Alignment.center,
  //           child: Icon(Icons.more_vert),
  //         ))
  //       ],
  //       hideHeader: true,
  //       title: Text("Please Select"),
  //       onConfirm: (Picker picker, List value) {
  //         print(value.toString());
  //         print(picker.getSelectedValues());
  //       }).showDialog(context);
  // }


}
